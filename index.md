---
layout: cv
title: Andrew Smith
---

# Andrew Smith

CTO, Software Engineer

<i class="fa-solid fa-envelope"></i> [me@espadav8.co.uk](me@espadav8.co.uk)

<i class="fa-brands fa-linkedin"></i> [https://www.linkedin.com/in/espadav8/](https://www.linkedin.com/in/espadav8/)

## Summary

I am a highly experienced and innovative tech lead with a background in software development, engineering management, and strategic planning. I have over 15 years of experience building highly performing teams, and a proven record of developing scalable SaaS solutions.

Currently taking a sabbatical after the acquisition of intelliHR by Humanforce to explore new opportunities and focus on professional development.

## Occupation

`Feb 2022 – present`
**Core Team member**, GitLab

- Joined the GitLab Core Team after working on a number of improvements in areas we used heavily at intelliHR, such as epics, roadmaps, and APIs

`Nov 2019 – Aug 2023`
**Chief Technology Officer**, intelliHR

Joined the company at a very early stage to build the next generation of the intelliHR platform, a completely new SaaS based product to replace the ageing on-premise solution. Saw the growth of the company from 3 team members to over 100.

- Led technical decision-making across the engineering department and the organisation
- Implemented SDLC policies to ensure consistent and high-quality software development practices
- Managed ISO 27001 and SOC2 Type 1 and Type 2 compliance certificates for the organisation
- Oversaw the engineering and support departments
- Restructured engineering into long-lived domain teams, enhancing ownership within the teams whilst enabling a deeper understanding of domains
- Introduced department-wide self-development hours for continuous learning
- Facilitated the purchase of intelliHR by Humanforce for AUD $77m
- Produced budgets and R&D reports, securing several million dollars in grants
- Streamlined developer operations by centralising tools into GitLab, improving development velocity and reducing time to deployment

`Mar 2015 – Nov 2019`
**Principal Software Engineer**, intelliHR

- Built the next generation of the intelliHR platform using the Laravel framework
- Scoped requirements, implemented technical solutions, led technical breakdowns, and researched technological implementations
- Conducted code reviews across the tech stack
- Developed and maintained CI/CD pipelines, Docker containers, and other developer experience tools
- Fostered a culture of continual self-development

`Feb 2010 – Feb 2015`
**Senior Developer**, BCM Partnership

- Developed large-scale, award-winning websites with a mobile-first approach
- Involved in all development stages, from concept to delivery
- Utilised technologies including Laravel, Zend, PostgreSQL, jQuery, and Git

`Nov 2007 – Dec 2009`
**Lead Web Developer**, Palringo

- Developed the Palringo website using a custom in-house PHP framework
- Frontend built using relevant standards with HTML, CSS, and jQuery
- Administered the server OS (Ubuntu 6.06 LTS) and utilised technologies such as mod_magnet, Memcached, mod_proxy, Bash/PHP scripting, and Amazon S3

## Education

`2000–2003`
**University of Leicester**

- Software and Electronic Engineering BEng (Honours)

`1993–2000`
**Wilmslow High School**

- A-Levels, GCSEs

<!-- ### Footer

Last updated: May 2024 -->
